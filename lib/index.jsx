var React = require('react');
var Router = require('react-router');
var Route = Router.Route;
var App = require('./components/App.jsx');
var AssociationPage = require('./components/AssociationPage.jsx');
var CompositionPage = require('./components/CompositionPage.jsx');
var ResourcePage = require('./components/ResourcePage.jsx');

var routes = (
  <Route handler={App}>
    <Route path="association" handler={AssociationPage}/>
    <Route path="composition" handler={CompositionPage}/>
    <Route path="resource" handler={ResourcePage}/>
  </Route>
)

Router.run(routes, Router.HashLocation, function(Root) {
  React.render(
    <Root>

    </Root>,
    document.getElementById('reactContainer')
  );
});
