var React = require('react');

var OverlayMenu = React.createClass({
  getInitialState: function() {
    return {};
  },
  componentDidMount: function() {

  },
  componentWillUnmount: function() {

  },
  onCloseClick: function() {
    if (this.props.onCloseClick)
      this.props.onCloseClick();
  },
  render: function() {
    var closeClasses = "overlay-close-button " + (this.props.visible ? " visible" : "");
    var classes = "overlaymenu"  + (this.props.visible ? " visible" : "");
    return (
      <div className={classes}>
        <button className={closeClasses} onClick={this.onCloseClick}/>
        {this.props.children}
      </div>
    );
  }
});

module.exports = OverlayMenu;
