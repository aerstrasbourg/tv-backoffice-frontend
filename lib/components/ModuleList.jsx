var React = require('react');

var ModuleItem = require('./ModuleItem.jsx');
var Composition = require('../stores/Composition.js');

var ModuleList = React.createClass({
  getInitialState: function() {
    return {
      modules: Composition.getModules()
    };
  },
  componentDidMount: function() {

  },
  componentWillUnmount: function() {

  },
  onDragStart: function(event) {
    // event.dataTransfer.setData("module", {});
  },
  render: function() {
    return (
      <li className="modulelist">
        {this.state.modules.map(function(mod) {
          return (<ul draggable="true"><ModuleItem name={mod.name}/></ul>);
        })}
      </li>
    );
  }
});

module.exports = ModuleList;
