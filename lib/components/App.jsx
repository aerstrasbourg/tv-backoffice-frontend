var React = require('react');
var Navbar = require('react-bootstrap').Navbar;
var Nav = require('react-bootstrap').Nav;
var NavItem = require('react-bootstrap').NavItem;
var Router = require('react-router');
var RouteHandler = Router.RouteHandler;
var NotificationPanel = require('./NotificationPanel.jsx');

var App = React.createClass({
  getInitialState: function() {
    return {};
  },

  componentDidMount: function() {

  },
  componentWillUnmount: function() {

  },
  render: function() {
    return (
      <div className="app">
        <Navbar brand='EpiTV' inverse toggleNavKey={0} className="app-navbar">
          <Nav right eventKey={0}>
            <NavItem eventKey={1} href='#/association'>Association</NavItem>
            <NavItem eventKey={2} href='#/composition'>Composition</NavItem>
            <NavItem eventKey={3} href='#/resource'>Resources</NavItem>
          </Nav>
        </Navbar>
        <div className="app-page">
          <RouteHandler/>
        </div>
        <NotificationPanel/>
      </div>
    );
  }
});

module.exports = App;
