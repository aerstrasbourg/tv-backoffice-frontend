var React = require('react');

var OverlayMenu = require('./OverlayMenu.jsx');
var ResourceCreationMenu = require('./ResourceCreationMenu.jsx');

var AddButton = React.createClass({
  getInitialState: function() {
    return {
      activated: false
    };
  },
  componentDidMount: function() {

  },
  componentWillUnmount: function() {

  },
  onClick: function() {
    this.setState({
      activated: true
    });
  },
  closing: function() {
    this.setState({
      activated: false
    });
  },
  render: function() {
    return (
      <span>
        <button className="addbutton" onClick={this.onClick}>

        </button>
        <OverlayMenu visible={this.state.activated} onCloseClick={this.closing}>
          <ResourceCreationMenu/>
        </OverlayMenu>
      </span>
    );
  }
});

module.exports = AddButton;
