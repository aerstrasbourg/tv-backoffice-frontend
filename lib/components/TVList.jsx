var React = require('react');
var Association = require('../stores/Association.js');
var TVCard = require('./TVCard.jsx');

var TVList = React.createClass({
  getInitialState: function() {
    return {
      TVs: Association.getTVs()
    };
  },
  componentDidMount: function() {

  },
  componentWillUnmount: function() {

  },
  render: function() {
    return (
      <span>{this.state.TVs.map(function(tv) {
        return (<TVCard tvId={tv.id} name={tv.name} img={tv.img}/>);
      })}</span>
    );
  }
});

module.exports = TVList;
