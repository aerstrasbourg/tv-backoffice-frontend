var React = require('react');

var ResourceList = require('./ResourceList.jsx');
var OverlayMenu = require('./OverlayMenu.jsx');

var ResourcePage = React.createClass({
  getInitialState: function() {
    return {};
  },
  componentDidMount: function() {

  },
  componentWillUnmount: function() {

  },
  render: function() {
    return (
      <div className="resource-page">
        <ResourceList/>
      </div>
    );
  }
});

module.exports = ResourcePage;
