var React = require('react');

var CompositionPreview = require('./CompositionPreview.jsx');
var ModuleList = require('./ModuleList.jsx');
var ModuleConfig = require('./ModuleConfig.jsx');

var CompositionPage = React.createClass({
  getInitialState: function() {
    return {};
  },
  componentDidMount: function() {

  },
  componentWillUnmount: function() {

  },
  render: function() {
    return (
      <div className="composition-page">
        <div className="left">
          <CompositionPreview/>
        </div>
        <div className="side">
          <ModuleConfig/>
          <ModuleList/>
        </div>
      </div>
    );
  }
});

module.exports = CompositionPage;
