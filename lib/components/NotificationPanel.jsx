var React = require('react');

var Notification = require('./Notification.jsx');
var NotificationStore = require('../stores/Notification.js');
var moment = require('moment');

var NotificationPanel = React.createClass({
  getInitialState: function() {
    return {
      notifications: NotificationStore.getNotifications()
    };
  },
  componentDidMount: function() {
    NotificationStore.addNewNotificationListener(this.addNotification);
    var that = this;
    setInterval(function() {
      for (var i = 0; i < that.state.notifications.length; ++i) {
        if (moment.duration(moment().diff(that.state.notifications[i].date)).seconds() > 5) {
          that.state.notifications.splice(0, 1);
          that.forceUpdate();
        }
      }
    }, 1000);
  },
  componentWillUnmount: function() {
    NotificationStore.removeNewNotificationListener(this.addNotification);
  },
  addNotification: function() {
    var notifs = this.state.notifications;
    var storedNotifs = NotificationStore.getNotifications();
    notifs.push(storedNotifs[storedNotifs.length - 1]);
    this.setState({
      notifications: notifs
    })
  },
  render: function() {
    return (
      <div className="notificationpanel">
        {this.state.notifications.map(function(notif) {
          return (<Notification message={notif.message}/>);
        })}
      </div>
    );
  }
});

module.exports = NotificationPanel;
