var React = require('react');

var CompositionItem = React.createClass({
  getInitialState: function() {
    return {};
  },
  componentDidMount: function() {

  },
  componentWillUnmount: function() {

  },
  onDragStart: function(event) {
    event.dataTransfer.setData("compoId", this.props.compoId);
  },
  render: function() {
    return (
      <div className="compositionitem" draggable="true" onDragStart={this.onDragStart}>
        {this.props.name}
      </div>
    );
  }
});

module.exports = CompositionItem;
