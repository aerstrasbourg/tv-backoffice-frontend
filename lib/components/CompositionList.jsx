var React = require('react');

var CompositionItem = require('./CompositionItem.jsx');
var Composition = require('../stores/Composition.js');

var CompositionList = React.createClass({
  getInitialState: function() {
    return {
      compositions: Composition.getCompositions()
    };
  },
  componentDidMount: function() {
    Composition.addNewCompoListener(this.getCompos);
  },
  componentWillUnmount: function() {
    Composition.removeNewCompoListener(this.getCompos);
  },
  getCompos: function() {
    this.setState({
      compositions: Composition.getCompositions()
    });
  },
  render: function() {
    return (
      <li className="composition-list">
        {this.state.compositions.map(function(compo) {
          return (<ul><CompositionItem name={compo.name} compoId={compo.id}/></ul>);
        })}
      </li>
    );
  }
});

module.exports = CompositionList;
