var React = require('react');

var ResourceCard = require('./ResourceCard.jsx');
var Resource = require('../stores/Resource.js');
var AddButton = require('./AddButton.jsx');

var ResourceList = React.createClass({
  getInitialState: function() {
    return {
      resources: Resource.getResources()
    };
  },
  componentDidMount: function() {
    Resource.addNewResourceListener(this.refreshResources);
    Resource.addRemovedResourceListener(this.refreshResources);
  },
  componentWillUnmount: function() {
    Resource.removeNewResourceListener(this.refreshResources);
    Resource.removeRemovedResourceListener(this.refreshResources);
  },
  refreshResources: function() {
    this.setState({
      resources: Resource.getResources()
    });
  },
  render: function() {
    return (
      <div>
        {this.state.resources.map(function(res) {
          return (<ResourceCard resId={res.id} name={res.name}/>);
        })}
        <AddButton/>
      </div>
    );
  }
});

module.exports = ResourceList;
