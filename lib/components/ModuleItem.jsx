var React = require('react');

var ModuleItem = React.createClass({
  getInitialState: function() {
    return {};
  },
  componentDidMount: function() {

  },
  componentWillUnmount: function() {

  },
  onDragEnterCapture: function(event) {
    console.log('drag');
  },
  render: function() {
    return (
      <div className="moduleitem">
        {this.props.name}
      </div>
    );
  }
});

module.exports = ModuleItem;
