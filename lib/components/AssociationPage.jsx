var React = require('react');
var TVList = require('./TVList.jsx');

var CompositionList = require('./CompositionList.jsx');

var AssociationPage = React.createClass({
  getInitialState: function() {
    return {};
  },
  componentDidMount: function() {

  },
  componentWillUnmount: function() {

  },
  render: function() {
    return (
      <div className="association-page">
        <div className="left">
          <TVList/>
        </div>
        <div className="side">
          <CompositionList/>
        </div>
      </div>
    );
  }
});

module.exports = AssociationPage;
