var React = require('react');

var AppBar = React.createClass({
  getInitialState: function() {
    return {};
  },
  componentDidMount: function() {

  },
  componentWillUnmount: function() {

  },
  render: function() {
    return (
      <div className="appbar">
        <div className="bar">
          TV
        </div>
        <div>
          {this.props.children}
        </div>
      </div>
    );
  }
});

module.exports = AppBar;
