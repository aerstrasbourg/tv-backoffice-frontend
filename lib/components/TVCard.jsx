var React = require('react');

var Associate = require('../actions/Associate.js');

var TVCard = React.createClass({
  getInitialState: function() {
    return {};
  },
  componentDidMount: function() {

  },
  componentWillUnmount: function() {

  },
  preventDefault: function(event) {
    event.preventDefault();
  },
  onDrop: function(event) {
    Associate.create(event.dataTransfer.getData("compoId"), this.props.tvId);
  },
  render: function() {
    return (
      <div className="card tvcard" onDragOver={this.preventDefault} onDrop={this.onDrop}>
        <div className="img-container">
          <img src={this.props.img}/>
        </div>
        <div className="content">
          <p>
            {this.props.name}
          </p>
        </div>
      </div>
    );
  }
});

module.exports = TVCard;
