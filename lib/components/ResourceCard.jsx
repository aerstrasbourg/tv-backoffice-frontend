var React = require('react');

var RemoveResource = require('../actions/RemoveResource.js');

var ResourceCard = React.createClass({
  getInitialState: function() {
    return {};
  },
  componentDidMount: function() {

  },
  componentWillUnmount: function() {

  },
  close: function() {
    RemoveResource.create(this.props.resId);
  },
  render: function() {
    return (
      <div className="resourcecard">
        <img/>
        <label>{this.props.name}</label>
        <button type="button" onClick={this.close}></button>
      </div>
    );
  }
});

module.exports = ResourceCard;
