var React = require('react');

var NewResource = require('../actions/NewResource.js');

var ResourceCreationMenu = React.createClass({
  getInitialState: function() {
    return {};
  },
  componentDidMount: function() {

  },
  componentWillUnmount: function() {

  },
  validate: function() {
    NewResource.create(this.state.name, this.state.file);
    this.setState({});
  },
  onNameChanged: function(event) {
    var state = this.state;
    state.name = event.target.value;
    this.setState(state);
  },
  onFileChanged: function(event) {
    var state = this.state;
    state.file = event.target.value;
    this.setState(state);
  },
  render: function() {
    return (
      <form className="resourcecreationmenu card" onSubmit={this.validate}>
        <h1>Creation</h1>
        <input type="text" onChange={this.onNameChanged}/>
        <input type="file" onChange={this.onFileChanged}/>
        <input type="submit"/>
      </form>
    );
  }
});

module.exports = ResourceCreationMenu;
