var React = require('react');

var Notification = React.createClass({
  getInitialState: function() {
    return {};
  },
  componentDidMount: function() {
    
  },
  componentWillUnmount: function() {

  },
  render: function() {
    return (
      <div className="notification">
        {this.props.message}
      </div>
    );
  }
});

module.exports = Notification;
