var dispatcher = require('../dispatchers/dispatcher.js');
var assign = require('object-assign');
var EventEmitter = require('events').EventEmitter;
var Associate = require('../actions/Associate.js');
var Notification = require('./Notification.js');
var Composition = require('./Composition.js');

var TVs = [
  {
    id: "id1",
    name: "hall",
    img: "http://lorempicsum.com/simpsons/627/300/4"
  },
  {
    id: "id2",
    name: "couloir",
    img: "http://lorempicsum.com/simpsons/627/300/4"
  }
];

var associations = [];

function associate(compoId, tvId) {
  var compositions = Composition.getCompositions();
  for (var i = 0; i < associations.length; i++) {
    if (associations[i].tv.id == tvId) {
      if (associations[i].composition.id == compoId) {
        Notification.notifyInfo("Already Associated");
        return;
      } else {
        associations[i].composition.id = compoId;
        Notification.notifySuccess("Associated");
        return;
      }
    }
  }
  var tv;
  var compo;
  for (var i = 0; i < TVs.length; ++i) {
    if (TVs[i].id == tvId)
      tv = TVs[i];
  }
  for (var i = 0; i < compositions.length; ++i) {
    if (compositions[i].id == compoId)
      compo = compositions[i];
  }
  if (tv === undefined || compo === undefined) {
    Notification.notifyError("Bad Id");
    return;
  }
  associations.push({
    tv: tv,
    composition: compo
  });
  Notification.notifySuccess("Associated");
}

var Association = assign({}, EventEmitter.prototype, {

/*
  emitEvent: function() {
    this.emit(EVENT);
  },
*/
/*
  addChangeListener: function(cb) {
    this.on(EVENT, cb);
  },

  removeChangeListener: function(cb) {
    this.removeListener(EVENT, cb);
  },
*/

  associate: function(compoId, tvId) {
    associate(compoId, tvId);
  },

  getTVs: function() {
    return TVs.slice();
  },

  dispatcherId: dispatcher.register(function(payload) {
    switch(payload.actionType) {
      case "Associate":
        associate(payload.compositionId, payload.tvId);
        break;
    }
    return true;
  })
});

module.exports = Association;
