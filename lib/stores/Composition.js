var dispatcher = require('../dispatchers/dispatcher.js');
var assign = require('object-assign');
var EventEmitter = require('events').EventEmitter;

var compositions = [
  {
    id: "id1",
    name: "JPO"
  }
];

var modules = [{
  name: "AER"
}, {
  name: "Planning"
}, {
  name: "Projets"
}, {
  name: "Video"
}, {
  name: "Image"
}, {
  name: "Slide"
}, {
  name: "Mosaique"
}];

var NEW_COMPO = "NEW_COMPO";

var Composition = assign({}, EventEmitter.prototype, {

/*
  emitEvent: function() {
    this.emit(EVENT);
  },
*/
/*
  addChangeListener: function(cb) {
    this.on(EVENT, cb);
  },

  removeChangeListener: function(cb) {
    this.removeListener(EVENT, cb);
  },
*/
  emitNewCompo: function() {
    this.emit(NEW_COMPO);
  },

  addNewCompoListener: function(cb) {
    this.on(NEW_COMPO, cb);
  },

  removeNewCompoListener: function(cb) {
    this.removeListener(NEW_COMPO, cb);
  },

  getCompositions: function() {
    return compositions.slice();
  },

  getModules: function() {
    return modules.slice();
  },

  dispatcherId: dispatcher.register(function(payload) {
    switch(payload.actionType) {

    }
    return true;
  })
});

module.exports = Composition;
