var dispatcher = require('../dispatchers/dispatcher.js');
var assign = require('object-assign');
var EventEmitter = require('events').EventEmitter;

var resources = [{
  id: "resId1",
  name: "res1"
}, {
  id: "resId2",
  name: "res2"
}, {
  id: "resId3",
  name: "res3"
}];

function addResource(name, file) {
  resources.push({
    id: name,
    name: name
  });
  Resource.emitNewResourceEvent();
}

function removeResource(resId) {
  for (var i = 0; i < resources.length; ++i) {
    if (resources[i].id == resId) {
      resources.splice(i, 1);
      Resource.emitRemovedResourceEvent();
      return true;
    }
  }
  return false;
}

var REMOVED_RESOURCE = "REMOVED_RESOURCE";
var NEW_RESOURCE = "NEW_RESOURCE";

var Resource = assign({}, EventEmitter.prototype, {

/*
  emitEvent: function() {
    this.emit(EVENT);
  },
*/
/*
  addChangeListener: function(cb) {
    this.on(EVENT, cb);
  },

  removeChangeListener: function(cb) {
    this.removeListener(EVENT, cb);
  },
*/

  emitRemovedResourceEvent: function() {
    this.emit(REMOVED_RESOURCE);
  },

  addRemovedResourceListener: function(cb) {
    this.on(REMOVED_RESOURCE, cb);
  },

  removeRemovedResourceListener: function(cb) {
    this.removeListener(REMOVED_RESOURCE, cb);
  },

  emitNewResourceEvent: function() {
    this.emit(NEW_RESOURCE);
  },

  addNewResourceListener: function(cb) {
    this.on(NEW_RESOURCE, cb);
  },

  removeNewResourceListener: function(cb) {
    this.removeListener(NEW_RESOURCE, cb);
  },

  getResources: function() {
    return resources.slice();
  },

  dispatcherId: dispatcher.register(function(payload) {
    switch(payload.actionType) {
      case "RemoveResource":
        removeResource(payload.resId);
        break;
      case "NewResource":
        addResource(payload.name, payload.file);
        break;
    }
    return true;
  })
});

module.exports = Resource;
