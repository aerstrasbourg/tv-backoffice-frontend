var dispatcher = require('../dispatchers/dispatcher.js');
var assign = require('object-assign');
var EventEmitter = require('events').EventEmitter;
var moment = require('moment');

var ERROR = "ERROR";
var WARNING = "WARNING";
var INFO = "INFO";
var SUCCESS = "SUCCESS";

var notifications = [];

var NEW_NOTIFICATION = "NEW_NOTIFICATION";

var Notification = assign({}, EventEmitter.prototype, {

/*
  emitEvent: function() {
    this.emit(EVENT);
  },
*/
/*
  addChangeListener: function(cb) {
    this.on(EVENT, cb);
  },

  removeChangeListener: function(cb) {
    this.removeListener(EVENT, cb);
  },
*/
  addNewNotificationListener: function(cb) {
    this.on(NEW_NOTIFICATION, cb);
  },

  removeNewNotificationListener: function(cb) {
    this.removeListener(NEW_NOTIFICATION, cb);
  },

  emitNewNotificationEvent: function() {
    this.emit(NEW_NOTIFICATION);
  },

  getNotifications: function() {
    return notifications.slice();
  },

  notify: function(message, type) {
    notifications.push({
      date: moment(),
      type: type,
      message: message
    });
    this.emitNewNotificationEvent();
  },

  notifyError: function(message) {
    this.notify(message, ERROR);
  },

  notifyInfo: function(message) {
    this.notify(message, INFO);
  },

  notifyWarning: function(message) {
    this.notify(message, WARNING);
  },

  notifySuccess: function(message) {
    this.notify(message, SUCCESS);
  },

  dispatcherId: dispatcher.register(function(payload) {
    switch(payload.actionType) {

    }
    return true;
  })
});

module.exports = Notification;
