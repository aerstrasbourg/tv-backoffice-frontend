var dispatcher = require('../dispatchers/dispatcher.js');

var type = "Associate";

var Associate = {
  create: function(compoId, tvId) {
    dispatcher.dispatch({
      actionType: type,
      compositionId: compoId,
      tvId: tvId
    });
  }
};

module.exports = Associate;
