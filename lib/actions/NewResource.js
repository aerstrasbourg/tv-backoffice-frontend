var dispatcher = require('../dispatchers/dispatcher.js');

var type = "NewResource";

var NewResource = {
  create: function(name, file) {
    dispatcher.dispatch({
      actionType: type,
      name: name,
      file: file
    });
  }
};

module.exports = NewResource;
