var dispatcher = require('../dispatchers/dispatcher.js');

var type = "RemoveResource";

var RemoveResource = {
  create: function(resId) {
    dispatcher.dispatch({
      actionType: type,
      resId: resId
    });
  }
};

module.exports = RemoveResource;
